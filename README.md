# Evolve SVGs

Generates SVG versions of images by using a [genetic
algorithm](https://en.wikipedia.org/wiki/Genetic_algorithm) to generate
[Voronoi diagrams](https://en.wikipedia.org/wiki/Voronoi_diagram) and converts
them to SVG images.

## Development setup

Evolve SVGs is written in [Rust](https://www.rust-lang.org/) and uses the
[Yew](https://yew.rs) framework, with [trunk](https://trunkrs.dev/) for builds.

It uses [Voronoice](https://github.com/andreesteve/voronoice) for generating
Voronoi diagrams.

To set up for development,
[install Rust](https://www.rust-lang.org/tools/install), then run:

```bash
cargo install trunk wasm-bindgen-cli
rustup target add wasm32-unknown-unknown
```

## Build

To run tests:

```bash
cargo test
```

To start a development server:

```bash
trunk serve
```

Before committing code:

```bash
cargo fmt
```
