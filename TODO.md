+ Keep best individuals from prev. generation
+ Try euclidean distance
+ Display fitness
+ Publish on gitlab pages
- Allow adding your own image
- Allow downloading result
- Shrink added image to make it run faster
- Make it look nice:
    - 3x3 grid of SVGs
    - 16:9 default image
    - Info about algorithms etc. and privacy and link to source code
    - Initial loading screen
- Updates within a single generation
- Allow tuning performance:
    - population size
    - reference image size
    - SVG display update frequency
    - (Maybe) any display update frequency - use ShouldRender to skip
    - not sampling all pixels
- Allow tuning:
    - mutation rate (position and colour)
    - selection proportion
    - how many pixels are evaluated?
    - size of adjusted reference image

## Later, maybe
- Add and remove points as mutations
- Try Normal distributions, and generally get it to evolve nicely
- Fix voronoice crashes, if they re-occur
- Display generations/sec
