all: test

test:
	cargo fmt
	cargo test

run:
	cargo fmt
	trunk serve

deploy:
	trunk build

doc:
	rustup doc
	cargo doc --open
