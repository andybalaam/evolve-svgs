use num::clamp;
use rand::distributions::{Distribution, Uniform};
use rand::rngs::ThreadRng;
use rand::{thread_rng, Rng};
use tiny_skia::Pixmap;

use crate::generate_resvg::generate_resvg;
use crate::image_size::ImageSize;
use crate::model::{Fitness, Individual, PointAndColor};

const POPULATION_SIZE: usize = 9;
const ELITISM_FACTOR: usize = 3; // 4 means 0.25 of the population wins
const NUMBER_OF_VORONOI_POINTS: usize = 100;

pub fn random_population(image_size: &ImageSize) -> Vec<Individual> {
    (0..POPULATION_SIZE)
        .map(|_| random_individual(image_size))
        .collect()
}

pub fn evaluate(
    population: Vec<Individual>,
    image_size: &ImageSize,
    reference_pixmap: &Pixmap,
) -> Vec<(Fitness, Individual)> {
    let mut ret: Vec<(Fitness, Individual)> = population
        .into_iter()
        .map(|i| (fitness(&i, image_size, reference_pixmap), i))
        .collect();
    ret.sort_by_key(|fi| -fi.0);
    ret
}

pub fn run(
    population: &Vec<(Fitness, Individual)>,
    image_size: &ImageSize,
    reference_pixmap: &Pixmap,
) -> Vec<(Fitness, Individual)> {
    let best_individuals: Vec<&Individual> = population
        .iter()
        .take(POPULATION_SIZE / ELITISM_FACTOR)
        .map(|fi| &fi.1)
        .collect();

    let mut new_population: Vec<Individual> = Vec::new();

    let mut first = true;
    for _ in 0..ELITISM_FACTOR {
        for individual in &best_individuals {
            new_population.push(if first {
                first = false;
                clone(individual)
            } else {
                mutate(individual)
            });
        }
    }

    evaluate(new_population, image_size, reference_pixmap)
}

fn clone(individual: &Individual) -> Individual {
    individual.to_vec()
}

fn fitness(
    individual: &Individual,
    image_size: &ImageSize,
    reference_pixmap: &Pixmap,
) -> i64 {
    let svg = generate_resvg(individual, image_size);

    let mut pixmap = tiny_skia::Pixmap::new(
        image_size.width.ceil() as u32,
        image_size.height.ceil() as u32,
    )
    .unwrap();

    resvg::render(&svg, usvg::FitTo::Original, pixmap.as_mut());

    let mut dist = 0;
    for (p1, p2) in reference_pixmap.pixels().iter().zip(pixmap.pixels().iter())
    {
        let dr = distance(p1.red(), p2.red()) as f64;
        let dg = distance(p1.green(), p2.green()) as f64;
        let db = distance(p1.blue(), p2.blue()) as f64;
        dist += (dr * dr + dg * dg + db * db).sqrt() as i64;
    }

    -dist
}

fn distance(n1: u8, n2: u8) -> u8 {
    if n1 < n2 {
        n2 - n1
    } else {
        n1 - n2
    }
}

fn random_individual(image_size: &ImageSize) -> Individual {
    let mut rng = thread_rng();
    let unif_width = Uniform::new(0.0, image_size.width);
    let unif_height = Uniform::new(0.0, image_size.height);
    let unif_color = Uniform::new(0, 255);
    (0..NUMBER_OF_VORONOI_POINTS)
        .map(|_| {
            PointAndColor::from_coords(
                unif_width.sample(&mut rng),
                unif_height.sample(&mut rng),
                unif_color.sample(&mut rng),
                unif_color.sample(&mut rng),
                unif_color.sample(&mut rng),
            )
        })
        .collect()
}

fn mutate(individual: &Individual) -> Individual {
    let ret = individual.iter().map(mutate_point).collect();
    // TODO: duplicate or remove points
    ret
}

fn mutate_point(point_and_color: &PointAndColor) -> PointAndColor {
    let mut rng = thread_rng();
    // TODO: use lazy_static to reduce work here
    let coord_mutator: Uniform<f64> = Uniform::<f64>::new_inclusive(-0.5, 0.5);
    let color_mutator: Uniform<i16> = Uniform::<i16>::new_inclusive(-5, 5);

    let mut ret = point_and_color.clone();

    let mutate_what = rng.gen::<f64>();
    // 10% chance of this point changing in some way
    if mutate_what < 0.02 {
        ret.point.x = point_and_color.point.x + coord_mutator.sample(&mut rng);
    } else if mutate_what < 0.04 {
        ret.point.y = point_and_color.point.y + coord_mutator.sample(&mut rng);
    } else if mutate_what < 0.06 {
        ret.color.r =
            mutate_rgb(point_and_color.color.r, color_mutator, &mut rng);
    } else if mutate_what < 0.08 {
        ret.color.g =
            mutate_rgb(point_and_color.color.g, color_mutator, &mut rng);
    } else if mutate_what < 0.10 {
        ret.color.b =
            mutate_rgb(point_and_color.color.b, color_mutator, &mut rng);
    }
    ret
}

fn mutate_rgb(value: u8, dist: Uniform<i16>, rng: &mut ThreadRng) -> u8 {
    clamp(value as i16 + dist.sample(rng), 0, 255) as u8
}
