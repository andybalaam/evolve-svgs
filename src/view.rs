use yew::html;
use yew::html::Html;

use crate::model::Model;
use crate::msg::Msg;

pub fn view(model: &Model) -> Html {
    html! {
        <div>
            <p class="controls">
                <button onclick=model.link.callback(|_| Msg::Start) disabled=model.running>{"Start"}</button>
                <button onclick=model.link.callback(|_| Msg::Stop) disabled=!model.running>{"Stop"}</button>
                { if model.running {"Running..."} else { "Stopped."} }
            </p>
            <p class="info">
                <span>{ format!( "Generation: {}", model.generation) }</span>
                <span>{ format!( "Fitness: {}", model.fitness_value) }</span>
            </p>
            <p class="image">
                { "Reference: "}<img src=model.reference_data_url.clone() />
            </p>
            <p class="svgs">{ model.display_svgs.clone() }</p>
        </div>
    }
}
