use std::rc::Rc;

use crate::image_size::ImageSize;
use crate::model::Individual;
use crate::vor::voronoi;
use usvg::{
    self, Align, AspectRatio, Color, Fill, NodeExt, NodeKind, Paint, Path,
    PathData, PathSegment, Rect, ShapeRendering, Size, Svg, Transform, Tree,
    ViewBox, Visibility,
};

pub fn generate_resvg(individual: &Individual, image_size: &ImageSize) -> Tree {
    let v = voronoi(individual, image_size);
    let tree = Tree::create(Svg {
        size: Size::new(image_size.width, image_size.height).unwrap(),
        view_box: ViewBox {
            rect: Rect::new(0.0, 0.0, image_size.width, image_size.height)
                .unwrap(),
            aspect: AspectRatio {
                defer: false,
                align: Align::None,
                slice: false,
            },
        },
    });

    let mut root = tree.root();

    let cells = v
        .iter_cells()
        .enumerate()
        .map(|(n, cell)| (cell, individual[n].color));

    for (cell, color) in cells {
        let mut path_segments: Vec<PathSegment> = Vec::new();
        let mut start = true;
        for vertex in cell.iter_vertices() {
            path_segments.push(if start {
                start = false;
                PathSegment::MoveTo {
                    x: vertex.x,
                    y: vertex.y,
                }
            } else {
                PathSegment::LineTo {
                    x: vertex.x,
                    y: vertex.y,
                }
            });
        }

        let data = Rc::new(PathData(path_segments));

        let path = Path {
            id: String::new(),
            transform: Transform::new_translate(0.0, 0.0),
            visibility: Visibility::Visible,
            fill: Some(Fill::from_paint(Paint::Color(Color::new_rgb(
                color.r, color.g, color.b,
            )))),
            stroke: None,
            rendering_mode: ShapeRendering::CrispEdges,
            text_bbox: None,
            data,
        };
        root.append_kind(NodeKind::Path(path));
    }
    tree
}
