use voronoice::{BoundingBox, Point, Voronoi, VoronoiBuilder};

use crate::image_size::ImageSize;
use crate::model::Individual;

pub fn voronoi(individual: &Individual, image_size: &ImageSize) -> Voronoi {
    let bounding_box = BoundingBox::new(
        Point {
            x: image_size.width / 2.0,
            y: image_size.height / 2.0,
        },
        image_size.width,
        image_size.height,
    );

    VoronoiBuilder::default()
        .set_sites(individual.iter().map(|p| p.point.clone()).collect())
        .set_bounding_box(bounding_box)
        .set_lloyd_relaxation_iterations(4)
        .build()
        .unwrap()
}
