pub struct ImageSize {
    pub width: f64,
    pub height: f64,
}

impl ImageSize {
    pub fn new(width: f64, height: f64) -> Self {
        Self { width, height }
    }
}
