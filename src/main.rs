mod ga;
mod generate_resvg;
mod generate_yew_svgs;
mod image_size;
mod model;
mod msg;
mod update;
mod view;
mod vor;

use image::codecs::png::PngDecoder;
use image::imageops::FilterType;
use image::{DynamicImage, ImageOutputFormat};
use tiny_skia::Pixmap;
use yew::html::{ComponentLink, Html, ShouldRender};
use yew::Component;

use crate::generate_yew_svgs::generate_yew_svgs;
use crate::image_size::ImageSize;
use crate::model::Model;
use crate::msg::Msg;
use crate::update::update;
use crate::view::view;

const MAX_REFERENCE_SIZE: (u32, u32) = (100, 100);

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let ref_bytes: &[u8] = include_bytes!("default_reference.png");
        let ref_dec = PngDecoder::new(ref_bytes).unwrap();
        let ref_img = DynamicImage::from_decoder(ref_dec).unwrap();
        let ref_img_sized = ref_img.resize(
            MAX_REFERENCE_SIZE.0,
            MAX_REFERENCE_SIZE.1,
            FilterType::CatmullRom,
        );
        let mut ref_png_bytes: Vec<u8> = Vec::new();
        ref_img_sized
            .write_to(&mut ref_png_bytes, ImageOutputFormat::Png)
            .unwrap();
        let reference_pixmap = Pixmap::decode_png(&ref_png_bytes).unwrap();

        let reference_png_base64 = base64::encode(&ref_png_bytes);
        let reference_data_url =
            format!("data:image/png;base64,{}", reference_png_base64);

        let width = reference_pixmap.width() as f64;
        let height = reference_pixmap.height() as f64;
        let image_size = ImageSize::new(width, height);
        let population = ga::random_population(&image_size);

        let population =
            ga::evaluate(population, &image_size, &reference_pixmap);

        let display_svgs = generate_yew_svgs(&population, &image_size);

        Self {
            link,
            timeout: None,
            running: false,
            generation: 0,
            population,
            image_size,
            reference_pixmap,
            display_svgs,
            reference_data_url,
            fitness_value: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        update(self, msg)
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        view(self)
    }
}

fn main() {
    yew::start_app::<Model>();
}
