use rgb::RGB8;
use tiny_skia::Pixmap;
use voronoice::Point;
use yew::html::ComponentLink;
use yew::services::Task;
use yew::Html;

use crate::image_size::ImageSize;

#[derive(Clone, Debug)]
pub struct PointAndColor {
    pub point: Point,
    pub color: RGB8,
}

impl PointAndColor {
    pub fn from_coords(x: f64, y: f64, r: u8, g: u8, b: u8) -> PointAndColor {
        PointAndColor {
            point: Point { x, y },
            color: RGB8::new(r, g, b),
        }
    }
}

pub type Individual = Vec<PointAndColor>;

pub type Fitness = i64;

pub struct Model {
    pub link: ComponentLink<Self>,
    pub timeout: Option<Box<dyn Task>>,
    pub running: bool,
    pub generation: usize,
    pub population: Vec<(Fitness, Individual)>,
    pub image_size: ImageSize,
    pub reference_pixmap: Pixmap,
    pub display_svgs: Vec<Html>,
    pub reference_data_url: String,
    pub fitness_value: i64,
}
