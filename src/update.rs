use std::time::Duration;
use yew::html::ShouldRender;
use yew::services::TimeoutService;

use crate::ga;
use crate::generate_yew_svgs::generate_yew_svgs;
use crate::model::Model;
use crate::msg::Msg;

const UPDATE_COUNT_EVERY: usize = 1;
const UPDATE_SVGS_EVERY: usize = 1;

pub fn update(model: &mut Model, msg: Msg) -> ShouldRender {
    match msg {
        Msg::Start => {
            model.running = true;
            model.link.send_message(Msg::Tick);
            set_tick_timeout(model);
            true
        }
        Msg::Stop => {
            model.running = false;
            model.display_svgs =
                generate_yew_svgs(&model.population, &model.image_size);
            true
        }
        Msg::Tick => {
            if model.running {
                for _ in 0..UPDATE_COUNT_EVERY {
                    model.population = ga::run(
                        &model.population,
                        &model.image_size,
                        &model.reference_pixmap,
                    );
                }
                model.fitness_value = model.population[0].0;
                model.generation += UPDATE_COUNT_EVERY;
                if model.generation % UPDATE_SVGS_EVERY == 0 {
                    model.display_svgs =
                        generate_yew_svgs(&model.population, &model.image_size);
                }
                set_tick_timeout(model);
                true
            } else {
                false
            }
        }
    }
}

fn set_tick_timeout(model: &mut Model) {
    let handle = TimeoutService::spawn(
        Duration::from_secs(0),
        model.link.callback(|_| Msg::Tick),
    );
    model.timeout = Some(Box::new(handle));
}
