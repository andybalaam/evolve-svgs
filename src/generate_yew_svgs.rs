use rgb::RGB8;
use voronoice::VoronoiCell;
use yew::html_nested;
use yew::Html;

use crate::image_size::ImageSize;
use crate::model::{Fitness, Individual};
use crate::vor::voronoi;

pub fn generate_yew_svgs(
    population: &Vec<(Fitness, Individual)>,
    image_size: &ImageSize,
) -> Vec<Html> {
    population
        .iter()
        .take(9)
        .map(|(_score, individual)| {
            individual_to_yew_svg(individual, image_size)
        })
        .collect()
}

fn individual_to_yew_svg(
    individual: &Individual,
    image_size: &ImageSize,
) -> Html {
    let v = voronoi(individual, image_size);

    let paths = v
        .iter_cells()
        .enumerate()
        .map(|(n, cell)| cell_as_svg_path(cell, individual[n].color));

    html_nested! {
        <svg
            width=image_size.width.to_string()
            height=image_size.height.to_string()
            viewBox=format!("0 0 {} {}", image_size.width, image_size.height)
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            { for paths }
        </svg>
    }
}

fn cell_as_svg_path(cell: VoronoiCell, color: RGB8) -> Html {
    let point_strings = cell
        .iter_vertices()
        .map(|p| format!("{} {}", p.x, p.y))
        .collect::<Vec<String>>();

    let d = format!("M{}", point_strings.join("L"));
    let fill = format!("#{:02X}{:02X}{:02X}", color.r, color.g, color.b);

    html_nested! {
        <path d=d fill=fill/>
    }
}
